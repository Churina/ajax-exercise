                                                           **AJAX exercise v1**

The backend team has decided on the data model for the API, allowing you to begin work on the frontend application that will consume it. Using their data model as a contract, we'll build out a mock API and develop our assets to use the specified format.
When the backend team eventually finishes building their API, we should be able to switch the address where we get the data and everything should work as expected. This allows both teams to work toward the contract simultaneously.
The first step is getting the API data to display in the UI so users can actually start to use the software (most people don't use Postman as their UI, despite what backend developers may tell you).

Data Model:
User: {
"id": number,
"name": string,
"image": URLstring
}

- Requirements:

1. Using mockAPI, create a mock server with a GET endpoint to retrieve an array of user objects to display. mockAPI allows you to either use random generated data or you can create custom JSON data.
2. The mockAPI GET endpoint should return at least 10 user objects following the data model above.
3. The page should display a card for each user which displays the user name and user image.
4. A loading spinner should be displayed when data is being fetched.
5. When the API call returns either a 4xx or 5xx error, an error message labelled Oops something went wrong should appear at the top of the screen or in a modal letting the user know an error occurred. 
6. Application should be broken down into multiple, logical components, stored inseparate files.
7. Should include layout and component styling using CSS-in-JS or CSS modules or CSS libraries (ex. Bootstrap or Pure.css). (No fully styled 3rd party components)

- Stretch Requirements (These are optional requirements for additional practice):

1. Create a second endpoint in mockAPI that returns a different type of data and display it in a similar way to the other data. The application should only display data after both calls have successfully returned, otherwise, show an error message.
