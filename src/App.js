import React, { useEffect,useCallback } from 'react'
import './App.css';
import Cards from './components/Cards.jsx'
import Image from './components/Image'
import Modal from './components/Modal';
import Spinner from './components/Spinner';

function App() {
   const [users,setUsers] = React.useState([]);
   const [loading, setLoading] = React.useState(true);
   const [error, setError] = React.useState("");

   useEffect(() => {
    setLoading(true);

    // use Async and await:
    const getData = async () => {
      try{
        const svrResp = await fetch('https://6412106e6e3ca3175305bffa.mockapi.io/users');
        
        if (svrResp.status>=400 && svrResp.status<=599) {
           setError('Oops Something went wrong!');
           setLoading(false);
        }
       
        else {
          const data = await svrResp.json();
          setUsers(data);
          setLoading(false);
        }
        
      } catch(e) {
        setError('Oops Something went wrong')
      }  
    }
    getData();
    
    // use fetch & .then: 
    // fetch('https://6412106e6e3ca3175305bffa.mockapi.io/users')
    // .then(response => {
    //   if(!response.ok){
    //     throw new Error('Oops Something went wrong');
    //   }
    //   return response.json();
    // })
    // .then(data => {
    //   setLoading(false);
    //   setUsers(data);
    // })
    // .catch(error =>{
    //   setError('Oops Something went wrong!');
    //   setLoading(false);
    // });

// practice callback function:
//    const main = ()=> {
//     console.log("callFromMain");
//    }

//    const child=(fn)=> {
//      console.log("callFromChild");
//      fn();
//    }

// const start = useCallback(()=> {
//    setTimeout(() => child(main), 1000);
 //const inter = setInterval(() => child(main), 1000);
 // clearInterval(inter);
}, [])

    const closeModal =()=>{
      setError(false);
    }

  return (

    <main className='page'>
      {error.length > 0 && <Modal message={error} closeModal={closeModal}/>}
      {loading && <Spinner pageStatus={loading} />}
      {users.map(user => (
        <Cards  key={user.id}>
            <Image image={user.image} />
            <h3>{user.name}</h3>
        </Cards>
      ))}
     
    </main>
  );
}

export default App;
