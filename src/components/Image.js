import React from 'react'

const Image = props => <img src={props.image} alt="images"/>

export default Image