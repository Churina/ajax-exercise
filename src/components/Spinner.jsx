import React from 'react'

const Spinner = ({pageStatus}) => 
    <>
        <div className="round"></div>
        <label>Loading... Please Wait!</label>
    </>
  
export default Spinner